﻿// <auto-generated />
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WKDB.Data.Context;

namespace WKDB.API.Migrations
{
    [DbContext(typeof(WKContext))]
    [Migration("20190411210346_Base")]
    partial class Base
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("WKDB.Data.Models.DB.Item", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<int>("Dexterity");

                    b.Property<bool>("Disabled");

                    b.Property<int>("Intelligence");

                    b.Property<int>("ItemType");

                    b.Property<int>("Luck");

                    b.Property<string>("Name");

                    b.Property<int>("Strength");

                    b.Property<int>("Vitality");

                    b.Property<int>("Wisdom");

                    b.HasKey("Id");

                    b.ToTable("Items");
                });
#pragma warning restore 612, 618
        }
    }
}
