using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WKDB.Data.Context;
using WKDB.Data.Models.DB;

namespace WKDB.API.Controllers
{
    public class ItemController : ControllerBase
    {
        private readonly WKContext _wkDb;

        public ItemController(WKContext wkDb)
        {
            _wkDb = wkDb;
        }

        [HttpGet("/api/items")]
        public ActionResult<IEnumerable<Item>> Get()
        {
            return _wkDb.Items.ToList();
        }
    }
}