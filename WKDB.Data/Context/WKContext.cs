using Microsoft.EntityFrameworkCore;
using WKDB.Data.Models.DB;

namespace WKDB.Data.Context
{
    public class WKContext : DbContext
    {
        public WKContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Item> Items { get; set; }
    }
}