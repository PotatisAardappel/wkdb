using Newtonsoft.Json;

namespace WKDB.Data.Models.Game
{
    public class BaseItem
    {
        [JsonProperty("ItemID")] public int ItemId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Disabled { get; set; }

        // Could be enum eventually, for w/e reason some itemType id exceed int32 limit...?
        public long ItemType { get; set; }

        public ItemStats Stats { get; set; }
    }
}