namespace WKDB.Data.Models.DB
{
    public class Item
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool Disabled { get; set; }

        // needs proper mapping
        public int ItemType { get; set; }

        public int Strength { get; set; }

        public int Dexterity { get; set; }

        public int Intelligence { get; set; }

        public int Vitality { get; set; }

        public int Luck { get; set; }

        public int Wisdom { get; set; }
    }
}