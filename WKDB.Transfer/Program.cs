﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using WKDB.Data.Context;
using WKDB.Data.Models.DB;
using WKDB.Data.Models.Game;

namespace WKDB.Transfer
{
    internal class Program
    {
        private const string Filepath = "baseitemdata.dat.json";

        // Nobody should do this
        private static WKContext GetWKContext()
        {
            var sC = new ServiceCollection();

            sC.AddDbContext<WKContext>(options => options.UseMySql(
                "Server=localhost;User Id=root;Password=root;Database=wkdb",
                b => b.MigrationsAssembly("WKDB.API")));

            var sP = sC.BuildServiceProvider();
            return sP.GetService<WKContext>();
        }
        
        public static void Main(string[] args)
        {
            if (!File.Exists(Filepath))
            {
                Console.WriteLine("Couldn't find file '{0}'", Filepath);
                Console.ReadKey();
            }

            StreamReader sR = null;
            FileStream fileStream = null;
            var jsonStr = "";
            var data = new List<BaseItem>();

            try
            {
                fileStream = File.Open(Filepath, FileMode.Open);
                sR = new StreamReader(fileStream);
                jsonStr = sR.ReadToEnd();
            }
            catch
            {
                // ignored
            }

            fileStream?.Dispose();
            sR?.Dispose();

            if (string.IsNullOrEmpty(jsonStr))
            {
                Console.WriteLine("File is empty");
                Console.ReadKey();
            }

            try
            {
                data = JsonConvert.DeserializeObject<List<BaseItem>>(jsonStr);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            var wkContext = GetWKContext();
            
            foreach (var gameItem in data)
            {
                wkContext.Items.Add(new Item
                {
                    Name = gameItem.Name,
                    Description = gameItem.Description,
                    Strength = gameItem.Stats.Strength,
                    Intelligence = gameItem.Stats.Intelligence
                    // etc etc....
                });
            }

            wkContext.SaveChanges();

            Console.WriteLine("We done now, saved {0} items, making the total items in DB {1}", data.Count, wkContext.Items.Count());
        }
    }
}