# WKDB

WonderKing DB, API and Parser based on some JSON

API contains the migrations required for DB as well as HTTP webhost

Data contains the structure and the DBContext (DB structure)

Transfer contains the transfer console (JSON to DB)


To run, start of by running ```dotnet ef database update``` from within the API project folder

Run transfer to transfer files

Run API webhost, navigate to http://localhost:5000/api/items to check stuff


TODO

Check whether moving DBContext to Data was even a good idea